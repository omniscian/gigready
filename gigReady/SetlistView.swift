//
//  SetlistView.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import SwiftUI

struct SetlistView: View {
    @ObservedObject var viewModel: SetlistViewModel
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    TextField("Search for artist", text: $viewModel.query)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    Button(action: viewModel.searchArtists) {
                        Image(systemName: "magnifyingglass")
                    }
                }
                .padding(.horizontal)

                if !viewModel.artists.isEmpty {
                    List(viewModel.artists) { artist in
                        Button(action: {
                            self.viewModel.selectedArtist = artist
                            self.viewModel.searchSetlists()
                        }) {
                            Text(artist.name)
                        }
                    }
                } else if viewModel.isLoading {
                    ProgressView()
                } else if let error = viewModel.error {
                    Text(error.localizedDescription)
                        .foregroundColor(.red)
                }
                
                if !viewModel.setlists.isEmpty {
                    List {
                        ForEach($viewModel.setlists) { $setlist in
                            VStack(alignment: .leading) {
                                Text(setlist.eventDate)
                                    .font(.headline)
                                Text(setlist.tour.name)
                                    .font(.headline)
                                Text(setlist.venue.name)
                                    .font(.subheadline)
                                Text(setlist.venue.city.name)
                                    .font(.subheadline)
                            }
                        }
                    }
                } else if viewModel.isLoading {
                    ProgressView()
                } else if let error = viewModel.error {
                    Text(error.localizedDescription)
                        .foregroundColor(.red)
                }
            }
            .navigationTitle("Setlists")
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SetlistView(viewModel: SetlistViewModel(artistService: ArtistService(apiKey: APIKeys().setlistAPIKey),
                                                setlistService: SetlistService(apiKey: APIKeys().setlistAPIKey)))
    }
}
