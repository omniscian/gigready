//
//  GigReadyApp.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import SwiftUI

@main
struct GigReadyApp: App {
    private let apiKeys = APIKeys()
    
    var body: some Scene {
        WindowGroup {
            SetlistView(viewModel: SetlistViewModel(artistService: ArtistService(apiKey: apiKeys.setlistAPIKey),
                                                    setlistService: SetlistService(apiKey: apiKeys.setlistAPIKey)))
            .environmentObject(apiKeys)
        }
    }
}
