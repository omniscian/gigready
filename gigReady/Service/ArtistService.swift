//
//  ArtistService.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import Foundation

class ArtistService {
    private let baseURL = "https://api.setlist.fm/rest/1.0"
    private let apiKey: String
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    func searchArtists(query: String, completion: @escaping (Result<[Artist], Error>) -> Void) {
        let baseURL = "https://api.setlist.fm/rest/1.0"
        let parameters = [
            "artistName": query,
            "p": "1",
            "sort": "sortName"
        ]
        
        guard let url = URL(string: "\(baseURL)/search/artists?\(parameters.queryParameters)") else {
            completion(.failure(SetlistError.invalidURL))
            return
        }
        
        var request = URLRequest(url: url)
        request.addValue(apiKey, forHTTPHeaderField: "x-api-key")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(SetlistError.noData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let artistsResponse = try decoder.decode(ArtistsResponse.self, from: data)
                completion(.success(artistsResponse.artists))
            } catch {
                print(error.localizedDescription)
                completion(.failure(error))
            }
        }.resume()
    }
}

struct ArtistsResponse: Decodable {
    let type: String
    let itemsPerPage: Int
    let page: Int
    let total: Int
    let artists: [Artist]

    enum CodingKeys: String, CodingKey {
        case type
        case itemsPerPage
        case page
        case total
        case artists = "artist"
    }
}

struct Artist: Identifiable, Codable {
    var id: String
    let name: String
    let sortName: String
    let tmid: Int?
    let disambiguation: String?
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "mbid"
        case name
        case sortName
        case tmid
        case disambiguation
        case url
    }
}


extension Dictionary {
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                              String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                              String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part)
        }
        return parts.joined(separator: "&")
    }
}
