//
//  Setlist.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import Foundation

struct SetlistResponse: Codable {
    let type: String
    let setlist: [Setlist]
    let total: Int
    let page: Int
    let itemsPerPage: Int
}

struct Setlist: Codable, Identifiable {
    let id: String
    let versionId: String
    let eventDate: String
    let lastUpdated: String
    let artist: Artist
    let venue: Venue
    let tour: Tour
    let sets: Sets
    let url: String
}   

struct Venue: Codable {
    let id: String
    let name: String
    let city: City
    let url: String
}

struct City: Codable {
    let id: String
    let name: String
    let state: String?
    let stateCode: String?
    let coords: Coords
    let country: Country
}

struct Coords: Codable {
    let lat: Double
    let long: Double
}

struct Country: Codable {
    let code: String
    let name: String
}

struct Tour: Codable {
    let name: String
}

struct Sets: Codable {
    let set: [Set]
}

struct Set: Codable {
    let song: [Song]
    let encore: Int?
}

struct Song: Codable {
    let name: String
    let cover: Cover?
    let info: String?
}

struct Cover: Codable {
    let mbid: String
    let name: String
    let sortName: String
    let disambiguation: String?
    let url: String
}

