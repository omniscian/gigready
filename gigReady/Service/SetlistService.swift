//
//  SetlistService.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import Foundation

class SetlistService {
    private let baseURL = "https://api.setlist.fm/rest/1.0"
    private let apiKey: String
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    func getSetlists(for artistId: String, completion: @escaping (Result<[Setlist], Error>) -> Void) {
        guard let url = URL(string: "\(baseURL)/artist/\(artistId)/setlists") else {
            completion(.failure(SetlistError.invalidURL))
            return
        }

        var request = URLRequest(url: url)
        request.addValue(apiKey, forHTTPHeaderField: "x-api-key")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(.failure(SetlistError.noData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let setlistResponse = try decoder.decode(SetlistResponse.self, from: data)
                completion(.success(setlistResponse.setlist))
            } catch let jsonError as NSError {
                print("JSON decode failed: \(jsonError.localizedDescription)")
            }
        }.resume()
    }
}

enum SetlistError: Error {
    case invalidURL
    case invalidResponse
    case decodingError
    case networkError(Error)
    case apiError(String)
    case noData
    case unknownError
}

extension String {
    func similarity(to other: String) -> Double {
        let s1 = self.lowercased()
        let s2 = other.lowercased()

        if s1 == s2 {
            return 1.0
        }

        let pairs1 = wordLetterPairs(s1)
        let pairs2 = wordLetterPairs(s2)

        var intersection = 0
        let union = pairs1.count + pairs2.count

        for pair1 in pairs1 {
            for pair2 in pairs2 {
                if pair1 == pair2 {
                    intersection += 1
                    break
                }
            }
        }

        return Double(intersection) / Double(union)
    }

    private func wordLetterPairs(_ str: String) -> [String] {
        var allPairs = [String]()

        // Tokenize the string and iterate over each word
        let words = str.components(separatedBy: .whitespacesAndNewlines)
        for word in words {
            // Skip empty words
            if word.isEmpty {
                continue
            }

            // Add letter pairs for the current word
            let pairsInWord = letterPairs(word)
            allPairs.append(contentsOf: pairsInWord)
        }

        return allPairs
    }

    private func letterPairs(_ str: String) -> [String] {
        let chars = Array(str)
        let len = chars.count

        if len < 2 {
            return [str]
        }

        var pairs = [String]()
        for i in 0..<len-1 {
            pairs.append(String(chars[i...i+1]))
        }

        return pairs
    }
}
