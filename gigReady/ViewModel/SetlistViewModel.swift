//
//  SetlistViewModel.swift
//  GigReady
//
//  Created by Ian Layland-Houghton on 24/03/2023.
//

import Foundation
import SwiftUI

class SetlistViewModel: ObservableObject {
    private let artistService: ArtistService
    private let setlistService: SetlistService
    
    @Published var query = ""
    @Published var artists: [Artist] = []
    @Published var selectedArtist: Artist?
    @Published var setlists: [Setlist] = []
    @Published var isLoading = false
    @Published var error: Error?
    
    init(artistService: ArtistService, setlistService: SetlistService) {
        self.artistService = artistService
        self.setlistService = setlistService
    }
    
    func searchArtists() {
        isLoading = true
        artistService.searchArtists(query: query) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                
                switch result {
                case .success(let artists):
                    let sortedArtists = artists.sorted { $0.name.similarity(to: self?.query ?? "") > $1.name.similarity(to: self?.query ?? "") }
                    self?.artists = sortedArtists
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
    
    func searchSetlists() {
        guard let artistId = selectedArtist?.id else { return }
        isLoading = true
        setlistService.getSetlists(for: artistId) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                
                switch result {
                case .success(let setlists):
                    self?.setlists = setlists
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
}

